<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserGithubTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserData()
    {
        $this->post('/api/users/jukajr', []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'id',
            'login',
            'name',
            'avatar_url',
            'html_url'
        ]);
    }

    public function testReposByUuser()
    {
        $this->post('/api/users/jukajr/repo', []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            '*' => [
               'id',
               'name',
               'description',
               'html_url'
           ]
       ]);
    }
}
