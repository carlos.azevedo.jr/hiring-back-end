# Required
https://www.docker.com/

# Install

```bash
docker-compose run app bash
```

Dentro do container
```bash
/var/www/app composer install
```

Para sair do container
```bash
exit
```

Start serviços
```bash
docker-compose up
```

# Routes

```bash
post http://localhost:8080/api/users/{username}
post http://localhost:8080/api/users/{username}/repo
```

# Testes
```bash
docker-compose run app bash
/var/www/app/vendor/bin/phpunit
```