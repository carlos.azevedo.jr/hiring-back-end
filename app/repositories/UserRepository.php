<?php

namespace App\Repositories;

class UserRepository
{
    public function __construct()
    {
        $this->github = new \Github\Client();
    }


    public function dataUser($username) {
        try {
            $dados = $this->github->api('user')->show($username);
            
            return [
                "id" => $dados['id'],
                "login" => $dados['login'],
                "name" => $dados['name'],
                "avatar_url" => $dados['avatar_url'],
                "html_url" => $dados['html_url']
            ];
            
        } catch (\Exception $e) {
            return ['msg' => 'usuário não encontrado.'];
        }        
    }

    public function listReposDataByUser($username) {
        try {
            $repos = $this->github->api('user')->repositories($username);

            return array_map(function($dados) {
                return [
                    "id" => $dados['id'],
                    "name" => $dados['name'],
                    "description" => $dados['description'],
                    "html_url" => $dados['html_url'],
                ];
            }, $repos);

        } catch (Exception $e) {
            return ['msg' => 'usuário não encontrado.'];
        }
    }
}
