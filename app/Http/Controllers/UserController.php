<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;

class UserController extends Controller
{
    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }


    public function getUser($username) {
        $dados = $this->userRepository->dataUser($username);

        return response()->json($dados);
    }

    public function getListReposByUser($username) {
        $dados = $this->userRepository->listReposDataByUser($username);

        return response()->json($dados);
    }
}
