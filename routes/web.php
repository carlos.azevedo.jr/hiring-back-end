<?php

$router->group(['prefix' => 'api'], function () use ($router) {
	$router->post('/users/{username}', ['uses' => 'UserController@getUser']);
	$router->post('/users/{username}/repo', ['uses' => 'UserController@getListReposByUser']);
});